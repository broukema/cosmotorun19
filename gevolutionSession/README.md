**License**
Copyright (c) 2019,2020 Ahsan Nazer, Boud Roukema

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, expressed or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contact, tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.

*******************************************************
#Using the tutorial examples

### Step 0 (optional)
For those who have never coded in c++ a basic intro using three examples. Use **cppBasics.cpp** to gain enough familiarity
with the C++ language to be able to follow the gevolutions session discussions.

a) <iostream> replaces <stdio.h> for io

b) c++ template. A simple template function that can recieves two integers or floats and returns the maximum value of
the correct data type. (Gevolution uses function and class templates to define fields such as gravitational potential,
tensor fields etc on lattices, the intention is that the workshop participant recognizes template notation.)

c) A simple C++ class that has two member functions. Classes are a foreign concept for those who have only coded in C or Fortran 90.

d) [Overloaded operators](https://en.wikipedia.org/wiki/Operator_overloading). (Required if the participant wants to decode physics equations in gevolution. Only a superficial understanding is required.)

### Step 1 (Install required software)
Install gevolution, LATfield2, hdf5 and fftw.

### Step 2 (Run potForce.cpp, dft_forwards.cpp)
**Purpose**:
Learn how to use "Lattice", "Site", "Field", "PlanFFT" classes.

Gevolution calculates fourier transforms (it computes discrete fourier transforms) when setting up the simulation initial conditions.
It uses the four classes "Lattice", "Site", "Field" and "PlanFFT". These classes are inherited from the LATfield2 library. To be
able to understand equations coded in gevolution or modify gevolution, a working knowledge of these classes is required.

a) A simple example that differentiates phi=1/r (Newtonian potential for a point source placed at the center of the lattice, without units) to obtain Force=1/r^2 (without units).
Requires defining "Lattice", "Site", "Fields", and taking finite differences the LATfield2 way to compute the force from potential.

b) Use LATfield2 to compute FFT of cos(2pi/5 x)+ 3*cos(2pi/10 x) (a made up example) on the lattice.
Plot the FFT coefficients using python and identity the two corresponding frequency peaks.

### Step 3 (Run gevolution with default settings)
**Purpose**:
Run a simulation using gevolution.

(This step can be performed without going through steps 1 to 2)

### Step 4 (Decipher gevolution code to recover the equations in arXiv 1604.06065)
Purpose:
Recover underlying equations from gevolution code.

(In ic_basic.hpp the initial conditions for the simulation are set. The initial particle displacements are obtained
from the gradient of a scalar field. This equation is provided in arXiv 1604.06065 i.e the main gevolution paper. The workshop participant
should be able to identity this equation from the ic_basic.hpp and perhaps modify it.)
