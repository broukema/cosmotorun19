/*
 Discrete Fourier Transform example using LATfield2.
 https://stackoverflow.com/questions/25735153/plotting-a-fast-fourier-transform-in-python
 Take fft on the lattice and plot using python to verify that it works as expected.
 Purpose: Familiarity with FFT in LATfield2
 */
#include <iostream>
#include "LATfield2.hpp"

using namespace LATfield2;

int main(int argc, char **argv)
{
    int n,m;
    int halo = 1;
    int khalo =0;
    int dim = 3;
    int comp = 1;


    for (int i=1 ; i < argc ; i++ ){
                if ( argv[i][0] != '-' )
                        continue;
                switch(argv[i][1]) {
                        case 'n':
                                n = atoi(argv[++i]);
                                break;
                        case 'm':
                                m =  atoi(argv[++i]);
                                break;
                }
        }

    parallel.initialize(n,m);


    int latSize[dim] = {20,20,20};
    double monopole = 7.0;
    Lattice lat;
    lat.initialize(3,latSize,halo);
    double renormFFT=(double)lat.sites();


    //Real to complex fourier transform
    Lattice latKreal;
    latKreal.initializeRealFFT(lat, khalo);

    Site x(lat);
    rKSite kReal(latKreal);

    Field<Real> signal;
    signal.initialize(lat,comp); /* comp = number of components */

    /* Imag is the LATfield2 type for complex numbers */
    Field<Imag> signalK;
    signalK.initialize(latKreal,comp);

    /* Try to get back the original signal */
    Field<Real> signalRestored;
    signalRestored.initialize(lat,comp); /* comp = number of components */

    /* Prepare a plan for a forward FFT from &signal to &signalK */
    PlanFFT<Imag> planReal(&signal,&signalK);

    /* Prepare a plan for a backward FFT from &signalK to &signalRestored */
    PlanFFT<Imag> planRealRestored(&signalRestored,&signalK);

   for(x.first();x.test();x.next())
    {
        double r = pow(0.5*0. + x.coord(0) - lat.size(0)/2*0., 2);
        //r += pow(0.5*0. + x.coord(1) - lat.size(1)/2*0., 2);
        //r += pow(0.5*0. + x.coord(2) - lat.size(2)/2*0., 2);
        r = sqrt(r);
        signal(x,0)= monopole + (cos(r*2.*3.14159265359/10.)+3.*cos(r*2.*3.14159265359/5.));
        /*
	   signal(x, 1)=0.;
	   signal(x, 2)=0.;
	*/
    }

    /* Execute a forward FFT from &signal to &signalK */
   planReal.execute(FFT_FORWARD);

   planRealRestored.execute(FFT_BACKWARD);

   for(x.first();x.test();x.next()){
     signalRestored(x,0) /= renormFFT;
   };

   /* normalise */
   for(kReal.first(); kReal.test(); kReal.next())
     {
       signalK(kReal) /= renormFFT;
     }


   signal.save("./signal.txt");
   signalK.save("./signalK.txt");
   signalRestored.save("./signalR.txt");

   for(kReal.first(); kReal.test(); kReal.next())
     {
       COUT << kReal  << endl;
     }

   COUT << "*******************************" << endl;
   for(x.first(); x.test(); x.next())
     {
       COUT << x << endl;
     }

}
