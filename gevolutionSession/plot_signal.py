import numpy as np
from matplotlib import pylab as plt
from numpy.linalg import norm

signal  = np.loadtxt("signal.txt")
signalK = np.loadtxt("signalK.txt")

fig, ax = plt.subplots()
#To plot against the correct frequency use
ax.plot(np.linspace(0.0, 1./2., 11), np.sqrt(signalK[:,0]*signalK[:,0]+ signalK[:,1]*signalK[:,1])[0:11])
#Just plot without worrying about the frequencies if interested in seeing two peaks
#ax.plot(np.sqrt(signalK[:,0]*signalK[:,0]+ signalK[:,1]*signalK[:,1])[0:11])
plt.show()


