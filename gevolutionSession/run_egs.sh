#!/bin/bash

printf "Set LATFIELD_INCLUDE to your LATfield2 directory with .hpp files\n"
printf "and then comment out the exit line in this script.\n"
printf "You can compare the compile options with those for gevolution\n"
printf "and you may need to adapt/update the compile options for your system.\n"
exit 0

LATFIELD_INCLUDE=
hdf5_lib="/usr/lib/x86_64-linux-gnu/hdf5/openmpi/"
hdf5_inc="/usr/include/hdf5/openmpi/"
#LIB="-lfftw3.so -lm -lhdf5_openmpi -lgsl -lgslcblas"

mpic++ -std=c++11 -o potForce potForce.cpp -I${LATFIELD_INCLUDE} -DFFT3D -lfftw3 -lm
printf "Once compiled run this using:\n"
printf "mpirun -np 4 ./potForce -n 2 -m 2\n"

mpic++ -std=c++11 -o dft_forwards dft_forwards.cpp -I${LATFIELD_INCLUDE} -DFFT3D  -lfftw3 -lm
printf "Once compiled run this using:\n"
printf "mpirun -np 4 ./dft_forwards -n 2 -m 2\n"

