#!/bin/bash

printf "Run this after doing:\nmpirun -np 4 ./potForce -n 2 -m 2\n"
printf "and\n"
printf "mpirun -np 4 ./dft_forwards -n 2 -m 2\n"

pr --merge --omit-header --join-lines coords.txt phi.txt force.txt |awk '{print $3,$4}'| LC_NUMERIC=C graph -TX -L"phi(r)"

pr --merge --omit-header --join-lines coords.txt phi.txt force.txt |awk '{print $3,$7}'| LC_NUMERIC=C graph -TX -L"F\\sbz\\eb(r)"

printf "Click in the windows to close them.\n"

head -n 20 signal.txt | nl -ba | LC_NUMERIC=C graph -TX -L"signal(x)"

head -n 11 signalK.txt | nl -ba | awk '{print ($1-1),$2}' | LC_NUMERIC=C graph -TX -m0 -S 16 -L"Re(F(k\sbx\eb))"
head -n 11 signalK.txt | nl -ba |awk '{print ($1-1),$4}' |sed -e 's/i.//' | graph -TX -m 0 -S 16 -L"Im(F(k\sbx\eb))"

# Check if forward and reverse FFTs are numerically exact inverses:

printf "The following check should give 8000 ratios of output/input signal,\n"
printf "counting unique occurrences of each floating point ratio.\n"
printf "If you get '8000 1', then you have a bitwise exact inversion on your system.\n"

pr --join-lines --omit-header --merge signal.txt signalR.txt | \
    LC_NUMERIC=C awk '{print $2/$1}' | \
    sort |uniq -c
