import numpy as np
from matplotlib import pylab as plt
from numpy.linalg import norm

phi = np.loadtxt("phi.txt")
force = np.loadtxt("force.txt")
coords = np.loadtxt("coords.txt")
print type(force)
normForce = np.array(map(norm, force))
normCoords = np.array(map(norm, coords))
print force[0], force.shape, normForce.size
fig, ax = plt.subplots()
ax.plot(normCoords, phi)
ax.plot(normCoords, normForce)
#ax.set_yscale('log')
plt.show()


