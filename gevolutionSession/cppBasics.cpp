/*
    Enough C++ to get you started with Gevolution.
*/
/* C++ includes */
#include <iostream>
#include <typeinfo>

/* C includes */
#include <stdio.h> /* for printf */

using namespace std;

/****************************Example 1 starts ***************************************/
template <class T>
T GetMax (T a, T b) {
    T result;
    result = (a>b) ? a : b;
    return (result);
}

void eg1(){
    /*
     * Template functions
     See: http://www.cplusplus.com/doc/oldtutorial/templates/
     */
    int i = 2, j = 9, k;
    double x = 3.14159, y = 2.71828, z;
    k = GetMax<int>(i, j);
    z = GetMax<double>(x, y);
    cout << k << " of type " << typeid(k).name() << endl;
    cout << z << " of type " << typeid(z).name() << endl;
}

/****************************Example 2 starts ***************************************/
template <class T>
class Mypair {
  T a, b;
public:
  /* On the next line, Mypair is a constructor of the class Mypair:
     e.g. https://www.tutorialspoint.com/cplusplus/cpp_constructor_destructor.htm */
  Mypair (T first, T second)
  {
    a = first;
    b = second;
  };
  T getmax();
};

template  <class T>
T Mypair<T>::getmax(){
  T retval;
  retval = a>b? a:b;
  return retval;
}

void eg2(){
    /*
     * template classes
     */
    Mypair <int> myobject (4, 75);
    cout << myobject.getmax() << endl;
    return ;
}

/****************************Example 3 starts ***************************************/
class Complx{
    double x, y; //z = x+iy
    public:
        Complx(double x = 0., double y = 0.);
        Complx operator+(const Complx&) const;
        void print(){
          if(y >=0){
            cout << x << "+" << y << "i" << endl;
          }else{
            cout << x << "-" << -y << "i" << endl;
          };
        }
};

Complx::Complx(double a, double b){
    x = a;
    y = b;
}

Complx Complx::operator+(const Complx & c) const{
    Complx result;
    result.x = (this->x + c.x);
    result.y = (this->y + c.y);
    return result;
}

void eg3(){
    /*
    Overloading operators.
    See e.g. 
    https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.1.0/com.ibm.zos.v2r1.cbclx01/cplr318.htm
    */
    Complx z1(3, -4); //z1 = 3-4i
    Complx z2(1, 1); //z2 = 1+i
    Complx z = z1 + z2;
    z.print(); /* use the method 'print' defined above for the class Complx */
}



/**************************** Main function starts ***************************************/
int main () {
    /****** Example 0 starts***********************/
    //int a, A;
    //a = 1;
    //A = 2;
    //cout << "a = " << a << " A = " << A<<endl;
    /****** Example 0 ends ***********************/

    printf("Example 1:\n");
    eg1();
    printf("\n\n");

    printf("Example 2:\n");
    eg2();
    printf("\n\n");

    printf("Example 3:\n");
    eg3();
    printf("\n\n");

    return 0;
}
